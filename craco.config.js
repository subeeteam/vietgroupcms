const path = require("path");
const CracoAntDesignPlugin = require("craco-antd");
// eslint-disable-next-line import/no-extraneous-dependencies
const webpack = require("webpack");

module.exports = {
  plugins: [
    {
      plugin: CracoAntDesignPlugin,
      options: {
        customizeThemeLessPath: path.join(__dirname, "src/styles/theme.less"),
      },
    },
  ],
  webpack: {
    plugins: [
      new webpack.ProvidePlugin({
        "window.Quill": "quill",
      }),
    ],
  },
};
