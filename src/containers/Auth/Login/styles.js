import stylex from "@ladifire-opensource/stylex";

const styles = stylex.create({
  page: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
    backgroundImage: "assets/images/login-background.jpg",
  },
});

export default styles;
