import indexRoutes from "./indexRoutes";
import PublicRoutes from "./PublicRoute";
import PrivateRoutes from "./PrivateRoutes";

export { indexRoutes, PublicRoutes, PrivateRoutes };
