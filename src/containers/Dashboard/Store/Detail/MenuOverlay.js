import { Menu } from "antd";
import { useDispatch } from "react-redux";
import {
  deleteProductCosmeticRequest,
  setCosmeticProductModal,
  setTagProductArticleModalData,
} from "providers/CosmeticProductProvider/slice";
import { setPopup } from "providers/GeneralProvider/slice";
import { ARTICLE_PRODUCT_MODAL_TYPE } from "utils/constants";

const MenuOverlay = ({ record, isPermissionToEdit, refetchList }) => {
  const dispatch = useDispatch();
  const { _id, name, cosmeticProductId } = record;
  return (
    <Menu>
      <Menu.Item
        onClick={() => {
          dispatch(
            setCosmeticProductModal({
              visible: true,
              data: record,
              type: ARTICLE_PRODUCT_MODAL_TYPE.DETAIL,
            })
          );
        }}
      >
        Xem chi tiết
      </Menu.Item>
      {isPermissionToEdit && (
        <>
          <Menu.Item
            onClick={() => {
              dispatch(
                setCosmeticProductModal({
                  visible: true,
                  data: record,
                  type: ARTICLE_PRODUCT_MODAL_TYPE.EDIT,
                })
              );
            }}
          >
            Chỉnh sửa
          </Menu.Item>
          <Menu.Item
            onClick={() => {
              dispatch(
                setPopup({
                  isOpen: true,
                  data: {
                    okText: "Xác nhận",
                    cancelText: "Huy",
                    title: "Xác nhận xóa sản phẩm",
                    content: `Bạn có muốn xóa: ${name}`,
                    onOk: () => {
                      dispatch(
                        deleteProductCosmeticRequest({
                          ids: [_id],
                          cosmeticProductIds: [cosmeticProductId],
                        })
                      ).then(() => {
                        refetchList();
                      });
                    },
                  },
                })
              );
            }}
          >
            Xóa sản phẩm
          </Menu.Item>
          <Menu.Item
            onClick={() =>
              dispatch(
                setTagProductArticleModalData({
                  visible: true,
                  data: {
                    products: [record],
                    originalProducts: [record],
                  },
                })
              )
            }
          >
            Set tag
          </Menu.Item>
        </>
      )}
    </Menu>
  );
};

export default MenuOverlay;
