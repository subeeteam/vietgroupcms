import { Menu } from "antd";
import { useDispatch } from "react-redux";
import _isEmpty from "lodash/isEmpty";
import _uniq from "lodash/uniq";

import {
  deleteProductCosmeticRequest,
  setTagProductArticleModalData,
} from "providers/CosmeticProductProvider/slice";
import { setPopup } from "providers/GeneralProvider/slice";

const MenuAllOverlay = ({
  isPermissionToEdit,
  refetchList,
  selectedRowKeys,
  selectedRows,
  productCosmeticNameList,
  cosmeticProductIdList,
}) => {
  const dispatch = useDispatch();

  if (!isPermissionToEdit) return <></>;

  return (
    <Menu>
      <Menu.Item
        onClick={() =>
          dispatch(
            setPopup({
              isOpen: true,
              data: {
                okText: "Xác nhận",
                cancelText: "Huy",
                title: "Xác nhận xóa sản phẩm",
                content: `Bạn có muốn xóa: ${productCosmeticNameList.join(
                  ", "
                )}`,
                onOk: () =>
                  dispatch(
                    deleteProductCosmeticRequest({
                      ids: selectedRowKeys,
                      cosmeticProductIds: _uniq(cosmeticProductIdList),
                    })
                  ).then(() => {
                    refetchList();
                  }),
              },
            })
          )
        }
        disabled={_isEmpty(selectedRowKeys)}
      >
        Xóa sản phẩm
      </Menu.Item>
      <Menu.Item
        disabled={_isEmpty(selectedRowKeys)}
        onClick={() =>
          dispatch(
            setTagProductArticleModalData({
              visible: true,
              data: {
                products: selectedRows,
                originalProducts: selectedRows,
              },
            })
          )
        }
      >
        Set tag
      </Menu.Item>
    </Menu>
  );
};

export default MenuAllOverlay;
