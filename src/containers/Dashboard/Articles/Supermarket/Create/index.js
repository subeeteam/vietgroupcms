import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Row, Typography } from "antd";

import BackButton from "components/BackButton";
import SuperMarketEdit from "components/Supermarket/Edit";
import { STORE_TYPE } from "utils/constants";
import { createStoreRequest } from "providers/StoreProvider/slice";
import Helper from "utils/helpers";
import "./styles.less";
import moment from "moment";

const SupermarketDetail = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const initialValues = {
    name: "",
    open: 0,
    close: 0,
    banners: null,
    thumbnail: null,
    address: "",
    phone: "",
    startDay: "",
    endDay: "",
    description: "",
  };

  const handleSubmit = async (values) => {
    const latLng = await Helper.getLatLng(values.address, {
      lng: values.lng,
      lat: values.lat,
    });
    values.longitude = latLng.lng;
    values.latitude = latLng.lat;
    if (values.start) {
      values.customDiscountCondition = {
        start: values.start,
        end: values.end,
      };
    }
    dispatch(
      createStoreRequest({
        type: STORE_TYPE.MARKET,
        data: values,
      })
    ).then((data) => {
      navigate(`/supermarket/${data._id}`, { state: { canGoBack: true } });
    });
  };
  return (
    <div className="supermarket-detail-wrapper">
      <Row
        className="supermarket-detail-header"
        justify="space-between"
        align="middle"
      >
        <Row>
          <BackButton />
          <Typography.Title level={4}>Tạo siêu thị</Typography.Title>
        </Row>
      </Row>
      <div className="detail-wrapper">
        <SuperMarketEdit
          initialValues={initialValues}
          onSubmit={handleSubmit}
          isCreating
        />
      </div>
    </div>
  );
};

export default SupermarketDetail;
