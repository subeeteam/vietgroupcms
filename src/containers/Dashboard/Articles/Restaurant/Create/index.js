import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Row, Typography } from "antd";
import "./styles.less";
import BackButton from "components/BackButton";
import RestaurantEdit from "components/Restaurant/Edit";
import { STORE_TYPE } from "utils/constants";
import { createStoreRequest } from "providers/StoreProvider/slice";
import Helper from "utils/helpers";

const RestaurantCreate = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const initialValues = {
    name: "",
    open: 0,
    close: 0,
    banners: null,
    thumbnail: null,
    address: "",
    phone: "",
    startDay: "",
    endDay: "",
    description: "",
  };

  const handleSubmit = async (values) => {
    const latLng = await Helper.getLatLng(values.address, {
      lng: values.lng,
      lat: values.lat,
    });
    values.longitude = latLng.lng;
    values.latitude = latLng.lat;
    if (values.start) {
      values.customDiscountCondition = {
        start: values.start,
        end: values.end,
      };
    }
    dispatch(
      createStoreRequest({
        type: STORE_TYPE.RESTAURANT,
        data: values,
      })
    ).then((data) => {
      navigate(`/restaurant/${data._id}`, { state: { canGoBack: true } });
    });
  };
  return (
    <div className="restaurant-detail-wrapper">
      <Row
        className="restaurant-detail-header"
        justify="space-between"
        align="middle"
      >
        <Row>
          <BackButton />
          <Typography.Title level={4}>Tạo nhà hàng</Typography.Title>
        </Row>
      </Row>
      <div className="detail-wrapper">
        <RestaurantEdit
          initialValues={initialValues}
          onSubmit={handleSubmit}
          isCreating
        />
      </div>
    </div>
  );
};

export default RestaurantCreate;
