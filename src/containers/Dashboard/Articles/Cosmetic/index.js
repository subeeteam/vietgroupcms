import { Row, Typography, Button, Dropdown, Menu, Tabs } from "antd";
import _get from "lodash/get";
import _isEmpty from "lodash/isEmpty";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import { StoreColumn } from "components/Columns";
import TabHeader from "components/TabHeader";
import { useQuery } from "hooks";
import Table from "components/common/Table";
import { IconSeeMore } from "assets/images";
import { ROLES, STORE_TYPE } from "utils/constants";
import { setPopup } from "providers/GeneralProvider/slice";
import {
  getStoreListRequest,
  lockStoreRequest,
  unlockStoreRequest,
  pinPromotionalRequest,
  setSelectedIds,
  resetStoreState,
} from "providers/StoreProvider/slice";
import "./styles.less";
import i18n from "i18n";
import SearchBar from "./SearchBar";

const { TabPane } = Tabs;

const Cosmetic = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { list, total, isLoading, selectedRowKeys, selectedRows } = useSelector(
    (state) => state.store
  );

  const [searchObject, setSearch] = useQuery(getStoreListRequest, {
    arrayParams: ["status"],
    customParameter: {
      type: STORE_TYPE.COSMETIC,
    },
  });
  const roleCurrentUser = useSelector((state) =>
    _get(state, "auth.currentUser.role")
  );
  const isAdminPermission = [ROLES.SUPER_ADMIN, ROLES.ADMIN].includes(
    roleCurrentUser
  );

  const cosmeticIdList = [];
  const cosmeticNameList = [];
  selectedRows.map((item) => cosmeticIdList.push(item._id));
  selectedRows.map((item) => cosmeticNameList.push(item.name));

  const isDisableLockBtn = !!selectedRows.find(
    (item) => item.status === "lock"
  );
  const isDisableUnlockBtn = !!selectedRows.find(
    (item) => item.status === "active"
  );

  useEffect(
    () => () => {
      dispatch(resetStoreState());
    },
    [dispatch]
  );

  const menuAll = () => (
    <Menu>
      <Menu.Item
        disabled={isDisableLockBtn || _isEmpty(selectedRowKeys)}
        className={
          isDisableLockBtn || _isEmpty(selectedRowKeys)
            ? ""
            : "close-bussiness-btn"
        }
        onClick={() => {
          dispatch(
            setPopup({
              isOpen: true,
              data: {
                okText: "Xác nhận",
                cancelText: "Huy",
                title: "Xác nhận khóa doanh nghiệp",
                content: `Bạn có muốn khóa ${cosmeticNameList.join(", ")}`,
                onOk: () =>
                  dispatch(
                    lockStoreRequest({
                      storeIds: selectedRowKeys,
                    })
                  ).then(() =>
                    dispatch(
                      getStoreListRequest({
                        ...searchObject,
                        type: STORE_TYPE.COSMETIC,
                      })
                    )
                  ),
              },
            })
          );
        }}
      >
        {i18n.t("cosmetic.lockBussiness")}
      </Menu.Item>
      <Menu.Item
        className={
          isDisableUnlockBtn || _isEmpty(selectedRowKeys)
            ? ""
            : "unlock-bussiness-btn"
        }
        disabled={isDisableUnlockBtn || _isEmpty(selectedRowKeys)}
        onClick={() =>
          dispatch(
            setPopup({
              isOpen: true,
              data: {
                okText: "Xác nhận",
                cancelText: "Huy",
                title: "Xác nhận mở khóa doanh nghiệp",
                content: `Bạn có muốn mở khóa ${cosmeticNameList.join(", ")}`,
                onOk: () =>
                  dispatch(
                    unlockStoreRequest({
                      storeIds: selectedRowKeys,
                    })
                  ).then(() =>
                    dispatch(
                      getStoreListRequest({
                        ...searchObject,
                        type: STORE_TYPE.COSMETIC,
                      })
                    )
                  ),
              },
            })
          )
        }
      >
        {i18n.t("cosmetic.unlock")}
      </Menu.Item>
    </Menu>
  );

  const menu = (record) => {
    const { _id, status, name, pin } = record;
    return (
      <Menu>
        <Menu.Item
          onClick={() =>
            navigate(`/cosmetic/${_id}`, { state: { canGoBack: true } })
          }
        >
          {i18n.t("cosmetic.seeDetail")}
        </Menu.Item>
        {isAdminPermission && (
          <>
            <Menu.Item
              onClick={() => {
                dispatch(
                  setPopup({
                    isOpen: true,
                    data: {
                      okText: "Xác nhận",
                      cancelText: "Huy",
                      title: `Xác nhận ${
                        pin === 0 ? "ghim" : "bỏ ghim"
                      } doanh nghiệp`,
                      content: `Bạn có muốn ${
                        pin === 0 ? "ghim" : "bỏ ghim"
                      } ${name}`,
                      onOk: () =>
                        dispatch(
                          pinPromotionalRequest({
                            _id,
                            pin: pin === 0 ? 1 : 0,
                          })
                        ).then(() =>
                          dispatch(
                            getStoreListRequest({
                              ...searchObject,
                              type: STORE_TYPE.COSMETIC,
                            })
                          )
                        ),
                    },
                  })
                );
              }}
            >
              {pin === 0
                ? i18n.t("cosmetic.pinPromotional")
                : i18n.t("cosmetic.unpinPromotional")}
            </Menu.Item>
            <Menu.Item
              className={status === "active" ? "" : "unlock-bussiness-btn"}
              disabled={status === "active"}
              onClick={() => {
                dispatch(
                  setPopup({
                    isOpen: true,
                    data: {
                      okText: "Xác nhận",
                      cancelText: "Huy",
                      title: "Xác nhận mở khóa doanh nghiệp",
                      content: `Bạn có muốn mở khóa ${record.name}`,
                      onOk: () =>
                        dispatch(
                          unlockStoreRequest({
                            storeIds: [_id],
                          })
                        ).then(() =>
                          dispatch(
                            getStoreListRequest({
                              ...searchObject,
                              type: STORE_TYPE.COSMETIC,
                            })
                          )
                        ),
                    },
                  })
                );
              }}
            >
              {i18n.t("cosmetic.unlock")}
            </Menu.Item>
            <Menu.Item
              className={status === "lock" ? "" : "lock-bussiness-btn"}
              disabled={status === "lock"}
              onClick={() => {
                dispatch(
                  setPopup({
                    isOpen: true,
                    data: {
                      okText: "Xác nhận",
                      cancelText: "Huy",
                      title: "Xác nhận khóa doanh nghiệp",
                      content: `Bạn có muốn khóa ${record.name}`,
                      onOk: () =>
                        dispatch(
                          lockStoreRequest({
                            storeIds: [_id],
                          })
                        ).then(() =>
                          dispatch(
                            getStoreListRequest({
                              ...searchObject,
                              type: STORE_TYPE.COSMETIC,
                            })
                          )
                        ),
                    },
                  })
                );
              }}
            >
              {i18n.t("cosmetic.lockBussiness")}
            </Menu.Item>
          </>
        )}
      </Menu>
    );
  };

  const actionButtons = (record) => (
    <Dropdown overlay={() => menu(record)} placement="bottomLeft" arrow>
      <Button
        className="see-more-btn"
        type="ghost"
        icon={<img src={IconSeeMore} alt="see-more" />}
      />
    </Dropdown>
  );

  const rowSelection = {
    selectedRowKeys,
    onSelect: (record, selected, _selectedRows) => {
      dispatch(
        setSelectedIds({
          record,
          selected,
          selectedRows: _selectedRows,
        })
      );
    },
    onSelectAll: (selected, _selectedRows, changeRows) => {
      dispatch(
        setSelectedIds({
          selected,
          selectedRows: _selectedRows,
          changeRows,
        })
      );
    },
  };

  const columns = StoreColumn({});

  return (
    <div className="cosmetic-list-wrapper">
      <Row className="cosmetic-list-header" justify="space-between">
        <Typography.Title level={4}>Danh sách làm đẹp</Typography.Title>
        <Button
          size="large"
          type="primary"
          onClick={() =>
            navigate("/cosmetic/create", { state: { canGoBack: true } })
          }
        >
          Tạo Store
        </Button>
      </Row>
      <div className="content-container">
        <SearchBar />
        <Tabs
          className="cosmetic-tabs-container"
          activeKey={searchObject.subType}
          type="card"
          onChange={(key) => {
            setSearch(
              {
                subType: key,
                keyword: searchObject.keyword,
                status: searchObject.status,
                activeStatus: searchObject.activeStatus,
              },
              true
            );
          }}
        >
          {["", "hair", "spa", true, "pin"].map((value) => (
            <TabPane
              className="status-tab-pane"
              tab={<TabHeader status={value} total={100} />}
              key={value}
            >
              <Table
                rowSelection={rowSelection}
                scroll={{ x: "max-content" }}
                loading={isLoading}
                bordered
                columns={[
                  columns.id,
                  columns.email,
                  columns.phone,
                  columns.accountHolder,
                  columns.name,
                  columns.address,
                  columns.rating,
                  columns.status,
                  columns.pin,
                  columns.isPromotionActive,
                  columns.rewardBalance,
                  {
                    title: (
                      <Dropdown
                        overlay={isAdminPermission ? menuAll : ""}
                        placement="bottomLeft"
                        arrow={isAdminPermission}
                      >
                        <Button
                          size="large"
                          className="see-more-btn"
                          type="ghost"
                          icon={<img src={IconSeeMore} alt="see-more" />}
                        />
                      </Dropdown>
                    ),
                    width: 100,
                    render: (record) => actionButtons(record),
                  },
                ]}
                dataSource={list}
                rowKey={(data) => data._id}
                total={total}
                onRow={(record) => ({
                  onDoubleClick: () => {
                    navigate(`/cosmetic/${record._id}`, {
                      state: { canGoBack: true },
                    });
                  },
                })}
              />
            </TabPane>
          ))}
        </Tabs>
      </div>
    </div>
  );
};

export default Cosmetic;
