import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams, useNavigate } from "react-router-dom";
import DOMPurify from "dompurify";
import { Row, Button, Typography } from "antd";
import LoadingIndicator from "components/LoadingIndicator";
import "./styles.less";
import Helper from "utils/helpers";
import BackButton from "components/BackButton";
import { getNewsDetailRequest } from "providers/NewsProvider/slice";

const NewsDetail = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const detail = useSelector((state) => state.news.detail);
  const isLoading = useSelector((state) => state.news.isLoading);

  const { idNews } = useParams();

  useEffect(() => {
    dispatch(getNewsDetailRequest({ idNews }));
  }, [idNews, dispatch]);

  const createMarkup = (html) => ({
    __html: DOMPurify.sanitize(html, {
      ADD_TAGS: ["iframe"],
      ADD_ATTR: ["allow", "allowfullscreen", "frameborder", "scrolling"],
    }),
  });
  if (isLoading) {
    return <LoadingIndicator />;
  }
  return (
    <div className="news-detail-wrapper">
      <Row
        className="news-detail-header"
        justify="space-between"
        align="middle"
      >
        <Row>
          <BackButton />
          <Typography.Title level={4}>Tin tức chi tiết</Typography.Title>
        </Row>
        <Button
          size="large"
          type="primary"
          onClick={() =>
            navigate(`/news/${idNews}/edit`, { state: { canGoBack: true } })
          }
        >
          Cập nhật
        </Button>
      </Row>
      <div className="detail-wrapper">
        <div className="detail-title-wrapper">
          <div className="detail-title">{detail.title}</div>
          <div className="detail-time">
            <div className="time-dot" />
            {Helper.getTimeStamp(detail.createdAt)}
          </div>
        </div>
        <div
          className="content-wrapper"
          dangerouslySetInnerHTML={createMarkup(detail.content)}
        ></div>
      </div>
    </div>
  );
};

export default NewsDetail;
