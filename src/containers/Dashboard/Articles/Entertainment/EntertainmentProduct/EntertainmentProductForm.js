import { useMemo, useState, useRef } from "react";
import PropTypes from "prop-types";
import { Formik, Form } from "formik";
import { Col, Row, Button, Switch } from "antd";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import _get from "lodash/get";
import _isEmpty from "lodash/isEmpty";
import {
  CURRENCY,
  PROMOTION_TYPE,
  PROMOTION_TYPE_TEXT,
  ARTICLE_PRODUCT_MODAL_TYPE,
} from "utils/constants";
import { TextInput, ImageUpload, DatePicker, Select } from "components/common";
import EditArticleTypeModal from "components/EditArticleTypeModal";
import {
  addProductEntertainmentTypeRequest,
  deleteProductEntertainmentTypeRequest,
  editProductEntertainmentTypeRequest,
  getProductEntertainmentTypeRequest,
  getEntertainmentProductListRequest,
} from "providers/EntertainmentProductProvider/slice";
import { PlusIcon } from "assets/images";
import "./styles.less";
import i18n from "i18n";
import Helper from "utils/helpers";
import EditEntertainmentProductTypeForm from "./EditEntertainmentProductTypeForm";
import { createProductEntertainmentValidate } from "./validate";

const EntertainmentProductForm = ({
  initialValues,
  onSubmit,
  setEntertainmentProductModal,
  type,
  productEntertainmentTypeList,
  searchObject,
  setSearch,
}) => {
  const dispatch = useDispatch();
  const formikRef = useRef(null);
  const entertainmentId = useSelector((state) =>
    _get(state, "entertainment.detail._id")
  );
  const formattedProductEntertainmentTypeList = useMemo(
    () =>
      productEntertainmentTypeList.map((productType) => ({
        value: productType._id,
        label: productType.name,
      })),
    [productEntertainmentTypeList]
  );
  const [hasPromotion, setHasPromotion] = useState(
    initialValues.promotion > 0 ||
      initialValues.percentagePromotion > 0 ||
      false
  );
  const [canGoBack, setCanGoBack] = useState(true);
  const [isOpenModal, setIsOpenModal] = useState({});

  const handleAddProductEntertainmentType = () => {
    const newProductType = formikRef.current?.values.newProductType;
    if (!_isEmpty(newProductType)) {
      dispatch(
        addProductEntertainmentTypeRequest({
          entertainmentId,
          name: newProductType,
        })
      );
      formikRef.current?.setFieldValue("newProductType", "");
    }
  };

  const handleRemoveProductEntertainmentType = (event) => {
    if (canGoBack) {
      setCanGoBack(false);
    }
    const productTypeIds = _get(searchObject, "productTypeIds", []).filter(
      (item) => item !== event.value
    );
    dispatch(
      deleteProductEntertainmentTypeRequest({
        entertainmentId,
        id: event.value,
        name: event.label,
        searchObject: { ...searchObject, productTypeIds },
      })
    );
    setSearch({ ...searchObject, productTypeIds });
    if (formikRef.current?.values.productType === event.value) {
      formikRef.current?.setFieldValue("productType", "");
    }
  };

  const handleEditProductEntertainmentType = (event) => {
    dispatch(
      editProductEntertainmentTypeRequest({
        entertainmentId,
        productTypeEntertainmentId: isOpenModal.value,
        name: event.name,
      })
    ).then(() => {
      dispatch(
        getProductEntertainmentTypeRequest({
          entertainmentId,
          limit: 1000,
          page: 1,
        })
      );
      dispatch(getEntertainmentProductListRequest(searchObject));
      setIsOpenModal({});
    });
  };

  const handleOpenEditTypeModal = (data) => {
    setIsOpenModal(data);
  };

  const formattedInitValues = {
    name: _get(initialValues, "name", ""),
    thumbnail: initialValues.thumbnail
      ? { url: initialValues.thumbnail, objectId: initialValues.thumbnail }
      : null,
    productType: _get(initialValues, "productType._id", ""),
    element: _get(initialValues, "element", ""),
    amount: _get(initialValues, "amount", ""),
    price: _get(initialValues, "price", ""),
    currency: _get(initialValues, "currency", CURRENCY.JPY),
    promotion: _get(initialValues, "promotion", 0),
    percentagePromotion: _get(initialValues, "percentagePromotion", 0),
    discountType:
      initialValues.promotion > 0
        ? PROMOTION_TYPE.CASH
        : PROMOTION_TYPE.PERCENT,
    fromDatePromotion: initialValues.fromDatePromotion
      ? moment(initialValues.fromDatePromotion)
      : null,
    toDatePromotion: initialValues.toDatePromotion
      ? moment(initialValues.toDatePromotion)
      : null,
  };

  const handlePromotionTypeChange = () => {
    formikRef.current?.setFieldValue("promotion", "0");
    formikRef.current?.setFieldValue("percentagePromotion", "0");
  };

  const handleDeletePromotion = () => {
    setHasPromotion(!hasPromotion);
    formikRef.current?.setFieldValue("promotion", "0");
    formikRef.current?.setFieldValue("percentagePromotion", "0");
  };
  return (
    <div className="entertainment-form-wrapper">
      {!_isEmpty(isOpenModal) && (
        <EditArticleTypeModal
          title={i18n.t("updateNameTypeModal.title")}
          data={isOpenModal}
          handleCloseModal={handleOpenEditTypeModal}
        >
          <EditEntertainmentProductTypeForm
            initialValues={{ name: isOpenModal.label }}
            onSubmit={handleEditProductEntertainmentType}
          />
        </EditArticleTypeModal>
      )}
      <Formik
        validateOnMount
        initialValues={formattedInitValues}
        enableReinitialize
        validationSchema={createProductEntertainmentValidate}
        innerRef={formikRef}
        onSubmit={onSubmit}
      >
        {({ handleSubmit, values }) => (
          <Form>
            <Row className="entertainment-product-form-container">
              <Col className="entertainment-product-thumbnail">
                <ImageUpload name="thumbnail" />
              </Col>
              <Col className="entertainment-product-form-info">
                <div className="m-title-text">
                  {i18n.t("entertainmentProduct.basicInfomation")}
                </div>
                <Row className="info-row">
                  <Col style={{ width: "100%" }}>
                    <div className="item-title">Tên sản phẩm:</div>
                    <TextInput name="name" size="small" required />
                  </Col>
                </Row>
                <Row className="info-row">
                  <Col span={11}>
                    <div className="item-title">Loại sản phẩm:</div>
                    <Select
                      name="productType"
                      required
                      options={formattedProductEntertainmentTypeList}
                      isRemove
                      handleRemoveItem={handleRemoveProductEntertainmentType}
                      isEditProductType
                      handleEditItem={handleOpenEditTypeModal}
                    />
                  </Col>
                  <Col span={11}>
                    <div className="item-title">Thêm mới loại:</div>
                    <div className="add-new-product-type">
                      <TextInput name="newProductType" size="small" />
                      <img
                        className="plus-icon"
                        src={PlusIcon}
                        alt=""
                        onClick={() => handleAddProductEntertainmentType()}
                      />
                    </div>
                  </Col>
                </Row>
                <Row className="info-row">
                  <Col style={{ width: "100%" }}>
                    <div className="item-title">Thành phần:</div>
                    <TextInput name="element" size="small" />
                  </Col>
                </Row>
                <Row className="info-row">
                  <Col style={{ width: "100%" }}>
                    <div className="item-title">Số lượng:</div>
                    <TextInput name="amount" size="small" required />
                  </Col>
                </Row>
                <Row className="info-row">
                  <Col span={11}>
                    <div className="item-title">Giá bán:</div>
                    <TextInput name="price" size="small" required />
                  </Col>
                  <Col span={11}>
                    <div className="item-title">Đơn vị:</div>
                    <Select
                      name="currency"
                      disabled
                      required
                      options={Helper.convertObjectToOptions(CURRENCY)}
                    />
                  </Col>
                </Row>
                <Row className="promotion-info-row">
                  <Switch
                    checked={hasPromotion}
                    disabled={Number(values.price) === 0}
                    onClick={() => handleDeletePromotion()}
                  />
                  <div className="promotion"> Khuyến mãi</div>
                </Row>
                {hasPromotion && (
                  <Row>
                    <Row className="info-row">
                      <Col span={11}>
                        <div className="item-title">Khuyến mãi:</div>
                        <Select
                          allowClear
                          name="discountType"
                          onChange={handlePromotionTypeChange}
                          options={Helper.convertObjectToOptions(
                            PROMOTION_TYPE_TEXT
                          )}
                          disabled={Number(values.price) === 0}
                        />
                      </Col>
                      <Col span={11}>
                        <div className="item-title">Giá Khuyến mãi:</div>
                        <TextInput
                          name={
                            formikRef.current?.values.discountType ===
                            PROMOTION_TYPE.CASH
                              ? "promotion"
                              : "percentagePromotion"
                          }
                          size="small"
                          required
                          disabled={Number(values.price) === 0}
                        />
                      </Col>
                    </Row>
                    <Row className="info-row">
                      <Col span={11}>
                        <div className="item-title">Từ ngày:</div>
                        <DatePicker
                          name="fromDatePromotion"
                          format="DD/MM/YYYY"
                          required
                          size="small"
                          disabled={
                            !(
                              values.promotion > 0 ||
                              values.percentagePromotion > 0
                            ) || Number(values.price) === 0
                          }
                        />
                      </Col>
                      <Col span={11}>
                        <div className="item-title">Đến ngày:</div>
                        <DatePicker
                          name="toDatePromotion"
                          format="DD/MM/YYYY"
                          required
                          size="small"
                          disabled={
                            !(
                              values.promotion > 0 ||
                              values.percentagePromotion > 0
                            ) || Number(values.price) === 0
                          }
                        />
                      </Col>
                    </Row>
                  </Row>
                )}
                {type === ARTICLE_PRODUCT_MODAL_TYPE.CREATE ? (
                  <Row className="add-product-actions">
                    <Button type="primary" size="small" onClick={handleSubmit}>
                      TẠO SẢN PHẨM
                    </Button>
                  </Row>
                ) : (
                  <Row className="save-back-product-actions">
                    <Button type="primary" size="small" onClick={handleSubmit}>
                      LƯU
                    </Button>
                    <Button
                      disabled={!canGoBack}
                      size="small"
                      onClick={() =>
                        dispatch(
                          setEntertainmentProductModal({
                            visible: true,
                            data: initialValues,
                            type: ARTICLE_PRODUCT_MODAL_TYPE.DETAIL,
                          })
                        )
                      }
                    >
                      TRỞ LẠI
                    </Button>
                  </Row>
                )}
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    </div>
  );
};

EntertainmentProductForm.propTypes = {
  initialValues: PropTypes.object,
  onTagChange: PropTypes.func,
  handleTagAllChange: PropTypes.func,
  tagAll: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  setEntertainmentProductModal: PropTypes.func,
  type: PropTypes.string,
  productEntertainmentTypeList: PropTypes.array,
  searchObject: PropTypes.object,
  setSearch: PropTypes.func,
};
export default EntertainmentProductForm;
