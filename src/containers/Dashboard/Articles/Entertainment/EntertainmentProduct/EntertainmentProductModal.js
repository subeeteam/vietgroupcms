import PropTypes from "prop-types";
import { Modal } from "antd";
import _get from "lodash/get";
import BackIcon from "assets/images/back.svg";
import { useSelector, useDispatch } from "react-redux";
import { ARTICLE_PRODUCT_MODAL_TYPE } from "utils/constants";
import { useParams } from "react-router-dom";
import {
  createProductEntertainmentRequest,
  getEntertainmentProductListRequest,
  updateProductEntertainmentRequest,
  deleteProductEntertainmentRequest,
} from "providers/EntertainmentProductProvider/slice";
import i18n from "i18n";
import EntertainmentProductDetail from "./EntertainmentProductDetail";
import EntertainmentProductForm from "./EntertainmentProductForm";
import "./styles.less";

const EntertainmentProductModal = (props) => {
  const {
    setEntertainmentProductModal,
    searchObject,
    isPermissionToEdit,
    setSearch,
  } = props;
  const dispatch = useDispatch();
  const entertainmentProductModalData = useSelector(
    (state) => state.entertainmentProduct.entertainmentProductModalData
  );
  const productEntertainmentTypeList = useSelector((state) =>
    _get(state, "entertainmentProduct.productEntertainmentTypeList")
  );

  const { visible, data = {}, type } = entertainmentProductModalData;
  const { entertainmentId } = useParams();
  const handleCloseModal = () => {
    dispatch(
      setEntertainmentProductModal({
        visible: false,
        data: {},
        type: "",
      })
    );
  };

  const handleEditEntertainmentProduct = (_data) => {
    dispatch(
      setEntertainmentProductModal({
        visible: true,
        data: _data,
        type: ARTICLE_PRODUCT_MODAL_TYPE.EDIT,
      })
    );
  };

  const handleDeleteEntertainmentProduct = () => {
    dispatch(
      deleteProductEntertainmentRequest({
        ids: [data._id],
        productEntertainmentIds: [data.productEntertainmentId],
      })
    ).then(() => {
      dispatch(
        setEntertainmentProductModal({
          visible: false,
          data: {},
          type: "",
        })
      );
      dispatch(getEntertainmentProductListRequest(searchObject));
    });
  };

  const handleSubmit = (values) => {
    const productId = data._id;
    const { productEntertainmentId } = data;
    if (type === ARTICLE_PRODUCT_MODAL_TYPE.CREATE) {
      dispatch(
        createProductEntertainmentRequest({
          ...values,
          entertainmentId,
        })
      ).then((_data) => {
        dispatch(
          setEntertainmentProductModal({
            visible: true,
            data: {
              ..._data,
              productType: productEntertainmentTypeList.find(
                (item) => item._id === _data.productType
              ),
            },
            type: ARTICLE_PRODUCT_MODAL_TYPE.DETAIL,
          })
        );
        dispatch(getEntertainmentProductListRequest(searchObject));
      });
    } else {
      dispatch(
        updateProductEntertainmentRequest({
          ...values,
          entertainmentId,
          productId,
          productEntertainmentId,
        })
      ).then((_data) => {
        dispatch(
          setEntertainmentProductModal({
            visible: true,
            data: {
              ..._data,
              productType: productEntertainmentTypeList.find(
                (item) => item._id === _data.productType
              ),
              productEntertainmentId,
            },
            type: ARTICLE_PRODUCT_MODAL_TYPE.DETAIL,
          })
        );
        dispatch(getEntertainmentProductListRequest(searchObject));
      });
    }
  };

  return (
    <div>
      {visible && (
        <Modal
          title={i18n.t("entertainmentProduct.productDetail")}
          visible={visible}
          width={860}
          className="entertainment-product-modal-wrapper"
          footer={null}
          onCancel={handleCloseModal}
          maskClosable={false}
          closeIcon={<img src={BackIcon} alt="" />}
        >
          {type === ARTICLE_PRODUCT_MODAL_TYPE.DETAIL ? (
            <EntertainmentProductDetail
              entertainmentProductDetail={data}
              setEntertainmentProductModal={setEntertainmentProductModal}
              handleDeleteEntertainmentProduct={
                handleDeleteEntertainmentProduct
              }
              handleEditEntertainmentProduct={handleEditEntertainmentProduct}
              isPermissionToEdit={isPermissionToEdit}
            />
          ) : (
            <EntertainmentProductForm
              type={type}
              setEntertainmentProductModal={setEntertainmentProductModal}
              productEntertainmentTypeList={productEntertainmentTypeList}
              searchObject={searchObject}
              onSubmit={handleSubmit}
              initialValues={data}
              setSearch={setSearch}
            />
          )}
        </Modal>
      )}
    </div>
  );
};
EntertainmentProductModal.propTypes = {
  setEntertainmentProductModal: PropTypes.func,
  searchObject: PropTypes.object,
  isPermissionToEdit: PropTypes.bool,
  setSearch: PropTypes.func,
};

export default EntertainmentProductModal;
