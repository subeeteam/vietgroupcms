import PropTypes from "prop-types";
import { Image, Col, Row, Button } from "antd";
import moment from "moment";
import DefaultImage from "assets/images/photo.png";

import i18n from "i18n";
import { PlaceHolderIMG } from "assets/images";

const EntertainmentProductDetail = (props) => {
  const {
    entertainmentProductDetail = {},
    handleEditEntertainmentProduct,
    handleDeleteEntertainmentProduct,
    isPermissionToEdit,
  } = props;
  const {
    thumbnail,
    name,
    productType,
    element,
    amount,
    price,
    percentagePromotion,
    promotion,
    fromDatePromotion,
    toDatePromotion,
    currency,
  } = entertainmentProductDetail;
  const renderItemInfo = (title, content) => (
    <Col className="info-col" span={6}>
      <div className="title-text">{title}</div>
      <div className="content-text">{content}</div>
    </Col>
  );

  return (
    <Row className="entertainment-product-detail-container">
      <Col className="entertainment-product-thumbnail">
        <Image
          width={200}
          height={150}
          fallback={PlaceHolderIMG}
          src={thumbnail || DefaultImage}
        />
        {(percentagePromotion > 0 || promotion > 0) && (
          <div className="sale-price-wrapper">
            -
            {promotion > 0
              ? `${Intl.NumberFormat("de-DE").format(promotion)} ${currency}`
              : `${percentagePromotion}%`}
          </div>
        )}
      </Col>
      <Col className="entertainment-product-detail-info">
        <div className="m-title-text">
          {i18n.t("entertainmentProduct.basicInfomation")}
        </div>
        <div className="detail-info-wrapper">
          <Row className="detail-info-row">
            {renderItemInfo("Tên sản phẩm:", name)}
            {productType && renderItemInfo("Loại:", productType.name)}
            {renderItemInfo("Số lượng:", amount)}
            {renderItemInfo("Thành phần:", `${element}`)}
          </Row>
          <Row className="detail-info-row">
            {renderItemInfo(
              "Giá bán:",
              `${Intl.NumberFormat("de-DE").format(price)} ${currency}`
            )}
            {renderItemInfo(
              "Khuyến mãi:",
              promotion > 0
                ? `${Intl.NumberFormat("de-DE").format(promotion)} ${currency}`
                : `${percentagePromotion}%`
            )}
            {(promotion > 0 || percentagePromotion > 0) && (
              <>
                {renderItemInfo(
                  "Từ ngày:",
                  moment(fromDatePromotion).format("DD/MM/YYYY")
                )}
                {renderItemInfo(
                  "Đến ngày:",
                  moment(toDatePromotion).format("DD/MM/YYYY")
                )}
              </>
            )}
          </Row>
        </div>
        {isPermissionToEdit && (
          <div className="edit-remove-product-actions">
            <Button
              type="primary"
              size="small"
              onClick={() =>
                handleEditEntertainmentProduct(entertainmentProductDetail)
              }
            >
              CHỈNH SỬA
            </Button>
            <Button
              type="primary"
              danger
              size="small"
              onClick={() => handleDeleteEntertainmentProduct()}
            >
              XÓA SẢN PHẨM
            </Button>
          </div>
        )}
      </Col>
    </Row>
  );
};
EntertainmentProductDetail.propTypes = {
  entertainmentDetail: PropTypes.object,
  setEntertainmentProductModal: PropTypes.func,
  handleEditEntertainmentProduct: PropTypes.func,
  handleDeleteEntertainmentProduct: PropTypes.func,
  isPermissionToEdit: PropTypes.bool,
  entertainmentProductDetail: PropTypes.object,
};

export default EntertainmentProductDetail;
