import { memo, useMemo } from "react";
import _get from "lodash/get";
import PropTypes from "prop-types";
import SearchBarComponent from "components/SearchBar";
import { Filter } from "components/common/Form";
import { useDispatch, useSelector } from "react-redux";
import { ARTICLE_PRODUCT_MODAL_TYPE } from "utils/constants";
import { Col, Row, Button } from "antd";
import { setEntertainmentProductModal } from "providers/EntertainmentProductProvider/slice";

const SearchBar = ({ isPermissionToEdit }) => {
  const dispatch = useDispatch();
  const productEntertainmentTypeList = useSelector((state) =>
    _get(state, "entertainmentProduct.productEntertainmentTypeList")
  );
  const formattedProductEntertainmentTypeList = useMemo(
    () =>
      productEntertainmentTypeList.map((productType) => ({
        value: productType._id,
        label: productType.name,
      })),
    [productEntertainmentTypeList]
  );
  return (
    <SearchBarComponent filterFields={["productTypeIds"]} isOnChangeSearch>
      <Col span={24} className="market-product-searchbar">
        <Row justify="space-between" align="middle">
          <Col className="l-title-text">Danh sách mặt hàng</Col>
          <Row>
            <Col flex="auto">
              {isPermissionToEdit && (
                <Button
                  type="primary"
                  size="large"
                  onClick={() =>
                    dispatch(
                      setEntertainmentProductModal({
                        visible: true,
                        data: {},
                        type: ARTICLE_PRODUCT_MODAL_TYPE.CREATE,
                      })
                    )
                  }
                >
                  Tạo sản phẩm
                </Button>
              )}
            </Col>
            <Col>
              <Filter
                name="productTypeIds"
                options={Object.values(formattedProductEntertainmentTypeList)}
                popConfirmClassName="gb-role-popconfirm"
                hasSearching
              />
            </Col>
          </Row>
        </Row>
      </Col>
    </SearchBarComponent>
  );
};

SearchBar.propTypes = {
  name: PropTypes.string,
  companyValue: PropTypes.string,
  isPermissionToEdit: PropTypes.bool,
};
export default memo(SearchBar);
