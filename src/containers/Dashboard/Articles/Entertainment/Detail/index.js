import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { Row, Typography, Menu, Dropdown, Button } from "antd";
import _isEmpty from "lodash/isEmpty";
import _uniq from "lodash/uniq";
import _get from "lodash/get";
import "./styles.less";
import BackButton from "components/BackButton";
import {
  getEntertainmentProductListRequest,
  setSelectedIds,
  deleteProductEntertainmentRequest,
  setEntertainmentProductModal,
  setTagProductArticleModalData,
  setTagProductEntertainmentRequest,
} from "providers/EntertainmentProductProvider/slice";
import EntertainmentComponent from "components/Entertainment/Detail";
import EntertainmentEdit from "components/Entertainment/Edit";
import Table from "components/common/Table";
import { useQuery } from "hooks";
import { EntertainmentProductColumn } from "components/Columns";
import i18n from "i18n";
import { IconSeeMore } from "assets/images";
import {
  ARTICLE_PRODUCT_MODAL_TYPE,
  ROLES,
  STORE_TYPE,
  DELETE_BANNER_CONTENT,
} from "utils/constants";
import { resetSelectedRowKey } from "providers/AdminProvider/slice";
import {
  setPopup,
  setChangeStoreOnwerModal,
} from "providers/GeneralProvider/slice";
import SetTagProductArticleModal from "components/SetTagProductArticleModal";
import ArticleShopOwner from "components/ArticleShopOwner";
import { Tabs } from "components/common";
import StoreBannerList from "components/StoreBanner/List";
import StoreBannerDetail from "components/StoreBanner/Detail";
import {
  createStoreBannerRequest,
  deleteStoreBannerRequest,
  editStoreBannerRequest,
  getStoreDetailRequest,
  updateStoreRequest,
  changeStoreOwnerRequest,
  getStoreBannerListRequest,
  resetStoreState,
} from "providers/StoreProvider/slice";
import Helper from "utils/helpers";
import EntertainmentProductModal from "../EntertainmentProduct/EntertainmentProductModal";
import SearchBar from "./SearchBar";

const EntertainmentDetail = () => {
  const dispatch = useDispatch();
  const [showEdit, setShowEdit] = useState(false);
  const { detail, isLoading, banners, isLoadingBanner } = useSelector(
    (state) => state.store
  );
  const [currentTab, setCurrentTab] = useState("detail");
  const [banner, setBanner] = useState({});
  const { userId = {} } = detail;
  const idCurrentUser = useSelector((state) =>
    _get(state, "auth.currentUser._id")
  );
  const roleCurrentUser = useSelector((state) =>
    _get(state, "auth.currentUser.role")
  );
  const isPermissionToEdit =
    idCurrentUser === userId._id ||
    [ROLES.SUPER_ADMIN, ROLES.ADMIN].includes(roleCurrentUser);
  const selectedRows = useSelector((state) =>
    _get(state, "entertainmentProduct.selectedRows", [])
  );
  const selectedRowKeys = useSelector((state) =>
    _get(state, "entertainmentProduct.selectedRowKeys", [])
  );
  const { entertainmentId } = useParams();
  const [searchObject, setSearch] = useQuery(
    getEntertainmentProductListRequest,
    {
      arrayParams: ["productTypeIds"],
      customParameter: {
        entertainmentId,
      },
    }
  );
  const productEntertainmentIdList = [];
  const productEntertainmentNameList = [];
  selectedRows.map((item) =>
    productEntertainmentIdList.push(item.productEntertainmentId)
  );
  selectedRows.map((item) => productEntertainmentNameList.push(item.name));

  useEffect(() => {
    if (entertainmentId) {
      dispatch(getStoreDetailRequest({ _id: entertainmentId }));
      dispatch(getStoreBannerListRequest({ storeId: entertainmentId }));
    }
    return () => {
      dispatch(resetStoreState());
    };
  }, [entertainmentId, dispatch]);

  const menuAll = () => (
    <Menu>
      <Menu.Item
        onClick={() =>
          dispatch(
            setPopup({
              isOpen: true,
              data: {
                okText: "Xác nhận",
                cancelText: "Huy",
                title: "Xác nhận xóa sản phẩm",
                content: `Bạn có muốn xóa: ${productEntertainmentNameList.join(
                  ", "
                )}`,
                onOk: () =>
                  dispatch(
                    deleteProductEntertainmentRequest({
                      ids: selectedRowKeys,
                      productEntertainmentIds: _uniq(
                        productEntertainmentIdList
                      ),
                    })
                  ).then(() =>
                    dispatch(getEntertainmentProductListRequest(searchObject))
                  ),
              },
            })
          )
        }
        disabled={_isEmpty(selectedRowKeys)}
      >
        {i18n.t("entertainmentProduct.delete")}
      </Menu.Item>
      <Menu.Item
        disabled={_isEmpty(selectedRowKeys)}
        onClick={() =>
          dispatch(
            setTagProductArticleModalData({
              visible: true,
              data: {
                products: selectedRows,
                originalProducts: selectedRows,
              },
            })
          )
        }
      >
        {i18n.t("products.setTag")}
      </Menu.Item>
    </Menu>
  );
  const menu = (record) => {
    const { _id, name, productEntertainmentId } = record;
    return (
      <Menu>
        <Menu.Item
          onClick={() =>
            dispatch(
              setEntertainmentProductModal({
                visible: true,
                data: record,
                type: ARTICLE_PRODUCT_MODAL_TYPE.DETAIL,
              })
            )
          }
        >
          {i18n.t("entertainmentProduct.seeDetail")}
        </Menu.Item>
        {isPermissionToEdit && (
          <>
            <Menu.Item
              onClick={() =>
                dispatch(
                  setEntertainmentProductModal({
                    visible: true,
                    data: record,
                    type: ARTICLE_PRODUCT_MODAL_TYPE.EDIT,
                  })
                )
              }
            >
              {i18n.t("entertainmentProduct.edit")}
            </Menu.Item>
            <Menu.Item
              onClick={() => {
                dispatch(
                  setPopup({
                    isOpen: true,
                    data: {
                      okText: "Xác nhận",
                      cancelText: "Huy",
                      title: "Xác nhận xóa sản phẩm",
                      content: `Bạn có muốn xóa: ${name}`,
                      onOk: () =>
                        dispatch(
                          deleteProductEntertainmentRequest({
                            ids: [_id],
                            productEntertainmentIds: [productEntertainmentId],
                          })
                        ).then(() =>
                          dispatch(
                            getEntertainmentProductListRequest(searchObject)
                          )
                        ),
                    },
                  })
                );
              }}
            >
              {i18n.t("entertainmentProduct.delete")}
            </Menu.Item>
            <Menu.Item
              onClick={() =>
                dispatch(
                  setTagProductArticleModalData({
                    visible: true,
                    data: {
                      products: [record],
                      originalProducts: [record],
                    },
                  })
                )
              }
            >
              {i18n.t("products.setTag")}
            </Menu.Item>
          </>
        )}
      </Menu>
    );
  };
  const actionButtons = (record) => (
    <Dropdown overlay={() => menu(record)} placement="bottomLeft" arrow>
      <Button
        className="see-more-btn"
        type="ghost"
        icon={<img src={IconSeeMore} alt="see-more" />}
      />
    </Dropdown>
  );

  const entertainmentProductList = useSelector((state) =>
    _get(state, "entertainmentProduct.docs")
  );
  const entertainmentProductTotal = useSelector((state) =>
    _get(state, "entertainmentProduct.totalDocs")
  );
  const visible = useSelector(
    (state) => state.entertainmentProduct.setTagProductArticleModalData.visible
  );
  const products = useSelector((state) =>
    _get(
      state.entertainmentProduct.setTagProductArticleModalData,
      "data.products",
      []
    )
  );
  const originalProducts = useSelector((state) =>
    _get(
      state.entertainmentProduct.setTagProductArticleModalData,
      "data.originalProducts",
      []
    )
  );

  const rowSelection = {
    selectedRowKeys,
    onSelect: (record, selected, _selectedRows) => {
      dispatch(
        setSelectedIds({ record, selected, selectedRows: _selectedRows })
      );
    },
    onSelectAll: (selected, _selectedRows, changeRows) => {
      dispatch(
        setSelectedIds({ selected, selectedRows: _selectedRows, changeRows })
      );
    },
  };

  const columns = [
    EntertainmentProductColumn.productId,
    EntertainmentProductColumn.name,
    EntertainmentProductColumn.type,
    EntertainmentProductColumn.element,
    EntertainmentProductColumn.amount,
    EntertainmentProductColumn.promotion,
    EntertainmentProductColumn.price,
    EntertainmentProductColumn.promotionPrice,
    EntertainmentProductColumn.tag,
    {
      title: (
        <Dropdown
          overlay={isPermissionToEdit ? menuAll : ""}
          placement="bottomLeft"
          arrow={isPermissionToEdit}
        >
          <Button
            size="large"
            className="see-more-btn"
            type="ghost"
            icon={<img src={IconSeeMore} alt="see-more" />}
          />
        </Dropdown>
      ),
      width: 100,
      render: (record) => actionButtons(record),
    },
  ];

  const handleSubmit = async (values) => {
    const latLng = await Helper.getLatLng(values.address, {
      lng: values.lng,
      lat: values.lat,
    });
    values.longitude = latLng.lng;
    values.latitude = latLng.lat;

    if (
      values.discountPercentage?.value === detail?.discountPercentage?.value &&
      values.discountPercentage?.value
    ) {
      values.discountPercentage = detail?.discountPercentage?.value;
    }

    dispatch(
      updateStoreRequest({
        ...values,
        _id: entertainmentId,
        type: STORE_TYPE.ENTERTAINMENT,
      })
    ).then(() => {
      dispatch(getStoreDetailRequest({ _id: entertainmentId }));
      setShowEdit(false);
    });
  };

  const handleChangeStoreOnwer = ({ type, idNewOnwer }) => {
    dispatch(
      changeStoreOwnerRequest({
        type,
        _id: entertainmentId,
        ownerId: idNewOnwer,
      })
    ).then(() => {
      dispatch(resetSelectedRowKey());
      dispatch(getStoreDetailRequest({ _id: entertainmentId }));
      dispatch(setChangeStoreOnwerModal({ isOpen: false }));
    });
  };

  const handleChangeTab = (activeKey) => {
    setCurrentTab(activeKey);
    setBanner({});
  };

  const handleSubmitBanner = (values) => {
    const imageUrl = _get(values, "imageUrl.url", "");
    if (banner.imageUrl === null) {
      dispatch(
        createStoreBannerRequest({
          ...values,
          imageUrl,
          storeId: entertainmentId,
        })
      ).then(() => {
        dispatch(getStoreBannerListRequest({ storeId: entertainmentId }));
      });
    } else {
      dispatch(
        editStoreBannerRequest({
          ...values,
          imageUrl,
          storeId: entertainmentId,
          bannerId: banner._id,
        })
      ).then(() => {
        dispatch(getStoreBannerListRequest({ storeId: entertainmentId }));
      });
    }
    setBanner({});
  };

  const handleRemoveBanner = (record) => {
    dispatch(
      setPopup({
        isOpen: true,
        data: {
          ...DELETE_BANNER_CONTENT,
          onOk: () =>
            dispatch(
              deleteStoreBannerRequest({
                storeId: entertainmentId,
                bannerId: record._id,
              })
            ).then(() => {
              dispatch(getStoreBannerListRequest({ storeId: entertainmentId }));
            }),
        },
      })
    );
  };
  const handleToggleBannerStatus = (record) => {
    dispatch(
      editStoreBannerRequest({
        ...record,
        storeId: entertainmentId,
        bannerId: record._id,
        isActive: !record.isActive,
      })
    ).then(() => {
      dispatch(getStoreBannerListRequest({ storeId: entertainmentId }));
    });
  };

  return (
    <div className="entertainment-detail-wrapper">
      <SetTagProductArticleModal
        category="entertainment"
        setTagProductArticleModalData={setTagProductArticleModalData}
        visible={visible}
        products={_uniq(products)}
        originalProducts={_uniq(originalProducts)}
        setTagProductArticleRequest={setTagProductEntertainmentRequest}
        getProductArticleListRequest={getEntertainmentProductListRequest}
        searchObject={searchObject}
      />
      <EntertainmentProductModal
        isPermissionToEdit={isPermissionToEdit}
        searchObject={searchObject}
        setEntertainmentProductModal={setEntertainmentProductModal}
        setSearch={setSearch}
      />
      <Row
        className="entertainment-detail-header"
        justify="space-between"
        align="middle"
      >
        <Row>
          <BackButton />
          <Typography.Title level={4}>Thông tin giải trí</Typography.Title>
        </Row>
      </Row>
      <div className="detail-wrapper">
        <ArticleShopOwner
          category="entertainment"
          shopOwnerInfoDetail={_get(detail, "userId", {})}
          handleChangeStoreOnwer={handleChangeStoreOnwer}
        />
        <Tabs
          type="card"
          activeKey={currentTab}
          onChange={handleChangeTab}
          destroyInactiveTabPane
          contents={[
            {
              key: "detail",
              header: "Thông tin cơ bản",
              content: showEdit ? (
                <EntertainmentEdit
                  initialValues={detail}
                  onCancel={() => setShowEdit(false)}
                  onSubmit={handleSubmit}
                />
              ) : (
                <EntertainmentComponent
                  isPermissionToEdit={isPermissionToEdit}
                  detail={detail}
                  onEdit={() => setShowEdit(true)}
                />
              ),
            },
            {
              key: "banner",
              header: "Set banner",
              content: _isEmpty(banner) ? (
                <StoreBannerList
                  list={banners}
                  total={banners.length}
                  loading={isLoadingBanner}
                  handleOpenBannerDetail={(data) => setBanner(data)}
                  handleRemoveBanner={handleRemoveBanner}
                  handleToggleStatus={handleToggleBannerStatus}
                />
              ) : (
                <StoreBannerDetail
                  initialValues={banner}
                  isEditBanner={banner.imageUrl !== null}
                  handleCancel={() => setBanner({})}
                  handleSubmit={handleSubmitBanner}
                />
              ),
            },
          ]}
        />
        <div className="content-container">
          <SearchBar isPermissionToEdit={isPermissionToEdit} />
          <Table
            rowSelection={rowSelection}
            scroll={{ x: 1000 }}
            loading={isLoading}
            bordered
            columns={columns}
            dataSource={entertainmentProductList}
            rowKey={(data) => data._id}
            total={entertainmentProductTotal}
          />
        </div>
      </div>
    </div>
  );
};

export default EntertainmentDetail;
