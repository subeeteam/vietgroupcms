import { useRef, useEffect } from "react";
import { Row, Button, Typography } from "antd";
import { Formik, Form } from "formik";
import _get from "lodash/get";
import { useParams, useNavigate } from "react-router-dom";
import { TextInput, TextEditor, ImageUpload } from "components/common";
import { useDispatch, useSelector } from "react-redux";
import BackButton from "components/BackButton";
import {
  getSimPostDetailRequest,
  updateSimPostRequest,
} from "providers/SimProvider/slice";
import { createVisaValidate } from "./validate";
import "./styles.less";

const SimPostEdit = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const isCreating = useSelector((state) => _get(state, "Visa.isCreating"));
  const detail = useSelector((state) => state.sim.simPostDetail);
  const { idPost } = useParams();
  useEffect(() => {
    dispatch(getSimPostDetailRequest({ idPost }));
  }, [idPost, dispatch]);

  const handleUpdateVisa = (values) => {
    dispatch(
      updateSimPostRequest({
        ...values,
        idPost,
        status: detail.status,
      })
    ).then(() =>
      navigate(`/sim/post/${idPost}`, {
        replace: true,
        state: { canGoBack: true },
      })
    );
  };
  const formikRef = useRef();

  return (
    <div className="Visa-update-page">
      <Row className="Visa-update-header" justify="space-between">
        <Row>
          <BackButton />
          <Typography.Title level={4}>Cập nhật Bài đăng SIM</Typography.Title>
        </Row>
        <Button
          loading={isCreating}
          size="large"
          type="primary"
          onClick={() => formikRef.current.submitForm()}
        >
          Cập nhật
        </Button>
      </Row>
      <div className="update-form-wrapper">
        <Formik
          validateOnMount
          innerRef={formikRef}
          enableReinitialize
          initialValues={{ ...detail, thumbnail: { url: detail.thumbnail } }}
          validationSchema={createVisaValidate}
          onSubmit={handleUpdateVisa}
        >
          {() => (
            <Form>
              <Row justify="space-between">
                <TextInput name="title" placeholder="Tiêu đề" size="large" />
              </Row>
              <Row justify="space-between" className="editor-wrapper">
                <ImageUpload name="thumbnail" />
                <TextEditor name="content" />
              </Row>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default SimPostEdit;
