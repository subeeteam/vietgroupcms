import { Link } from "react-router-dom";
import { AgentIcon } from "assets/images";
import i18n from "i18n";
import "./style.less";

const renderItem = (icon, text, to) => (
  <div className="item-wrapper">
    <Link to={to}>
      <img src={icon} alt="" />
      <div className="item-content">
        <div className="item-title">{text}</div>
      </div>
    </Link>
  </div>
);
const Home = () => (
  <div className="home-container content-wrapper">
    <div className="item-list-wrapper">
      {renderItem(AgentIcon, i18n.t("global.agents"), "/agents")}
    </div>
  </div>
);

export default Home;
