import AddressIcon from "./address.svg";
import DrinkIcon from "./alcoholic-drink.svg";
import AreaIcon from "./area.svg";
import BloodIcon from "./blood-type.svg";
import BuildingIcon from "./building-type.svg";
import CompanyIcon from "./company.svg";
import EligibilityIcon from "./eligibility.svg";
import FloorIcon from "./floor.svg";
import HobbyIcon from "./hobby.svg";
import LanguageIcon from "./language.svg";
import OriginIcon from "./origin-place.svg";
import PersonalityIcon from "./personality.svg";
import PhoneIcon from "./phone.svg";
import PriceIcon from "./price-range.svg";
import StudyIcon from "./study.svg";
import SuitcaseIcon from "./suite-case.svg";
import UserIcon from "./user.svg";
import MailIcon from "./mail.svg";
import Photo from "./photo.png";
import EmptyUser from "./empty.svg";
import AgentIcon from "./agent.svg";
import EmptyItem from "./emptyItem.svg";
import LoginBackground from "./login-background.jpg";
import IconFilter from "./IconFilter";
import IconChecked from "./IconChecked";
import SuccessIcon from "./SuccessIcon.png";
import FailIcon from "./FailIcon.png";
import OutDateIcon from "./OutDateIcon.png";
import CancelIcon from "./CancelIcon.png";
import SeemoreIcon from "./SeemoreIcon";
import IconSeeMore from "./icon-see-more.svg";
import LeftArrowIcon from "./left-arrow.svg";
import IconBirthdayCycle from "./icon-birthday-cycle.svg";
import IconEarth from "./icon-earth.svg";
import IconHeart from "./icon-heart.svg";
import IconHeartCycle from "./icon-heart-cycle.svg";
import IconHome from "./icon-home.svg";
import IconMailCycle from "./icon-mail-cycle.svg";
import IconPhoneCycle from "./icon-phone-cycle.svg";
import IconUserCycle from "./icon-user-cycle.svg";
import IconWork from "./icon-work.svg";
import FollowersCycle from "./followers.png";
import PlusIcon from "./plus-icon.svg";
import MessageIcon from "./message.svg";
import PlaceHolderIMG from "./placeholderImg.svg.png";
import StockExist from "./stockExist.svg";
import Removed from "./removed.svg";
import SoldOut from "./soldout.svg";

export {
  StockExist,
  Removed,
  SoldOut,
  PlaceHolderIMG,
  AddressIcon,
  DrinkIcon,
  AreaIcon,
  BloodIcon,
  BuildingIcon,
  CompanyIcon,
  EligibilityIcon,
  FloorIcon,
  HobbyIcon,
  LanguageIcon,
  OriginIcon,
  PersonalityIcon,
  PhoneIcon,
  PriceIcon,
  StudyIcon,
  SuitcaseIcon,
  UserIcon,
  MailIcon,
  Photo,
  EmptyUser,
  AgentIcon,
  EmptyItem,
  LoginBackground,
  IconFilter,
  IconChecked,
  SeemoreIcon,
  SuccessIcon,
  FailIcon,
  OutDateIcon,
  CancelIcon,
  IconSeeMore,
  LeftArrowIcon,
  IconBirthdayCycle,
  IconEarth,
  IconHeart,
  IconHeartCycle,
  IconHome,
  IconMailCycle,
  IconPhoneCycle,
  IconUserCycle,
  IconWork,
  FollowersCycle,
  PlusIcon,
  MessageIcon,
};
