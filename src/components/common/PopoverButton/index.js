import { memo } from "react";
import { Button, Dropdown, Menu } from "antd";
import { DashOutlined } from "@ant-design/icons";
import PropType from "prop-types";

const PopoverButton = (props) => {
  const { menus = [], handleMenuClick } = props;
  const menu = (
    <Menu onClick={(data) => handleMenuClick(data)}>
      {menus.map((_menu) => (
        <Menu.Item key={_menu.key} icon={_menu.icon}>
          {_menu.name}
        </Menu.Item>
      ))}
    </Menu>
  );
  return (
    <Dropdown trigger="click" overlay={menu}>
      <Button>
        <DashOutlined />
      </Button>
    </Dropdown>
  );
};

PopoverButton.propTypes = {
  menus: PropType.arrayOf(PropType.object).isRequired,
  handleMenuClick: PropType.func.isRequired,
};
export default memo(PopoverButton);
