import PropTypes from "prop-types";
import { Select } from "components/common";
import "./styles.less";

const SelectFromToDate = ({ startDayOptions, endDayOptions }) => (
  <div className="select-from-to-date-wrapper">
    <Select name="startDay" required options={startDayOptions} />
    <div className="form-to-text">đến</div>
    <Select name="endDay" required options={endDayOptions} />
  </div>
);

SelectFromToDate.propTypes = {
  initialValues: PropTypes.object,
  startDayOptions: PropTypes.array,
  endDayOptions: PropTypes.array,
};

export default SelectFromToDate;
