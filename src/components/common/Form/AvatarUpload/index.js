import { useState } from "react";

import _ from "lodash";
import { useField } from "formik";
import { Upload, message, Avatar } from "antd";
import { UserOutlined, LoadingOutlined } from "@ant-design/icons";
import ImgCrop from "antd-img-crop";
import Helper from "utils/helpers";

const AvatarUpload = (props) => {
  const [loading, setLoading] = useState(false);
  const [field, meta, helper] = useField(props);

  const image = !_.isEmpty(field.value)
    ? { uid: field.value.objectId, url: _.get(field.value, "url") }
    : {};

  const handleBeforeUpload = (file) => {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("You can only upload JPG/PNG file!");
    }
    const isLt5M = file.size / 1024 / 1024 < 5;
    if (!isLt5M) {
      message.error("Image must smaller than 5MB!");
    }
    return isJpgOrPng && isLt5M;
  };

  const customRequest = async (option) => {
    const { file } = option;
    setLoading(true);
    const avatarImage = await Helper.uploadFile({ file, type: "AVATAR" });
    if (avatarImage) {
      helper.setValue({ ...avatarImage, url: avatarImage.large.url });
      setLoading(false);
    }
  };
  return (
    <div>
      <ImgCrop shape="round">
        <Upload
          className="avatar-upload-container"
          listType="picture"
          showUploadList={false}
          beforeUpload={handleBeforeUpload}
          customRequest={customRequest}
        >
          <Avatar
            size={70}
            icon={loading ? <LoadingOutlined /> : <UserOutlined />}
            src={_.isEmpty(image) ? "" : image.url}
          />
          <div>{_.get(meta, "error", "")}</div>
        </Upload>
      </ImgCrop>
    </div>
  );
};

export default AvatarUpload;
