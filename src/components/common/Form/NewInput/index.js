import { forwardRef, memo } from "react";
import PropTypes from "prop-types";
import { useField } from "formik";
import { Input, Form } from "antd";
import MaskedInput from "antd-mask-input";
import { InfoCircleOutlined } from "@ant-design/icons";
import _get from "lodash/get";
import "./style.less";

const TextInput = forwardRef(
  (
    {
      label,
      maxLength,
      inputType,
      size = "large",
      allowClear = true,
      className = "",
      name,
      ...props
    },
    ref
  ) => {
    const [field, meta] = useField(name);
    const isError = meta.touched && meta.error;

    const renderInput = () => {
      if (inputType === "password") {
        return (
          <Input.Password
            maxLength={maxLength}
            size={size}
            allowClear={allowClear}
            ref={ref}
            {...field}
            {...props}
          />
        );
      }
      if (inputType === "textarea") {
        return (
          <Input.TextArea
            maxLength={maxLength}
            allowClear={allowClear}
            {...field}
            {...props}
            ref={ref}
          />
        );
      }
      if (inputType === "search") {
        return (
          <Input.Search maxLength={maxLength} {...field} {...props} ref={ref} />
        );
      }
      if (inputType === "maskInput") {
        return (
          <MaskedInput
            maxLength={maxLength}
            {...field}
            {...props}
            ref={ref}
            style={{ height: "40px" }}
          />
        );
      }
      return (
        <Input
          maxLength={maxLength}
          size={size}
          allowClear={allowClear}
          {...field}
          {...props}
          ref={ref}
        />
      );
    };
    return (
      <div className={`input-container text-field-container ${className}`}>
        <Form.Item
          label={label}
          validateStatus={isError ? "error" : ""}
          help={
            <div className="helper-wrapper">
              <div className="error-text">
                {isError && (
                  <>
                    <InfoCircleOutlined
                      className="info-icon"
                      type="info-circle"
                    />
                    {meta.error}
                  </>
                )}
              </div>
              {maxLength && (
                <div className="max-length-text">
                  {_get(field.value, "length", 0)}/{maxLength}
                </div>
              )}
            </div>
          }
        >
          {renderInput()}
        </Form.Item>
      </div>
    );
  }
);
TextInput.displayName = "TextInput";

TextInput.propTypes = {
  label: PropTypes.string,
  maxLength: PropTypes.number,
  inputType: PropTypes.string,
  size: PropTypes.string,
  allowClear: PropTypes.bool,
  className: PropTypes.string,
  name: PropTypes.string,
};

export default memo(TextInput);
