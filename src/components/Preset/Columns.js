/* eslint-disable react/display-name */

import { Link } from "react-router-dom";
import { Typography, Row, Col, Tooltip } from "antd";
import _get from "lodash/get";
import _isEmpty from "lodash/isEmpty";
import Tag from "components/Tag";
import TypeBlock from "components/TypeBlock";
// import { CheckCircleTwoTone } from '@ant-design/icons';
import PermissionTooltip from "components/PermissionTooltip";
import "./style.less";
import i18n from "i18n";
import { IconChecked } from "assets/images";

const Columns = {};

const renderDataValue = (value) => {
  const style = value ? {} : { color: "#c0c0c0" };
  return <span style={style}>{value || i18n.t("general.none")}</span>;
};

Columns.objectId = {
  dataIndex: "_id",
  key: "_id",
  title: "#",
  render: (_id) => renderDataValue(_id),
};

Columns.phone = {
  dataIndex: "phone",
  key: "phone",
  title: "Số điện thoại",
  render: (phone) => renderDataValue(phone),
};

Columns.displayName = {
  title: "Ten hien thi",
  dataIndex: "name",
  key: "name",
  sorter: true,
  render: (name) => renderDataValue(name),
};

Columns.permission = {
  title: "Phan quyen",
  dataIndex: "roles",
  key: "roles",
  render: (roles = []) =>
    renderDataValue(
      <Row>
        {roles.map((role) => (
          <Tooltip key={role} title={<PermissionTooltip />} color="white">
            <Col style={{ marginRight: 4 }}>
              <TypeBlock type={role} />
            </Col>
          </Tooltip>
        ))}
      </Row>
    ),
};

Columns.auth = {
  title: "Xac thuc",
  dataIndex: "active",
  key: "active",
  render: (active) => renderDataValue(active ? <IconChecked /> : <div />),
};

Columns.email = {
  title: i18n.t("listUserScreen.email"),
  dataIndex: "email",
  key: "email",
  render: (email) => renderDataValue(email),
};

Columns.companyName = {
  width: 160,
  title: i18n.t("global.company"),
  key: "companyId",

  render: (record) => (
    <Link
      onClick={(e) => e.stopPropagation()}
      to={`/companies/${_get(record, "company.objectId")}`}
    >
      {_get(record, "company.name")}
    </Link>
  ),
};

Columns.userStatus = {
  title: i18n.t("listUserScreen.status"),
  key: "status",
  width: 100,
  render: (record) => <Tag status="ACTIVE" />,
};

Columns.status = {
  dataIndex: ["createdBy", "companyName"],
  key: "status",
  width: 120,
  title: i18n.t("listUserScreen.status"),
  filters: [
    {
      value: "NEW",
      text: "New",
    },
    {
      value: "WAITING",
      text: "New",
    },
    {
      value: "VERIFIED",
      text: "Verified",
    },
  ],
};

Columns.actions = {
  key: "btnActions",
  width: 180,
  title: i18n.t("global.action"),
};

Columns.storeCreateActions = {
  key: "btnActions",
  width: 90,
  title: i18n.t("global.action"),
};

Columns.index = {
  key: "objectId",
  width: 60,
  title: "ID",
  render: (text, record, index) => index + 1,
};

Columns.storeAddress = {
  width: 180,
  title: i18n.t("global.storeAddress"),
  key: "address",
  render: (record) => {
    const { city, buildingName, prefecture } = record;
    return (
      <Typography.Paragraph
        ellipsis={{
          rows: 3,
          expandable: true,
          onExpand: (e) => e.stopPropagation(),
        }}
      >
        {!_isEmpty(buildingName) ? `${buildingName},` : ""} {city}, {prefecture}
      </Typography.Paragraph>
    );
  },
};

Columns.storeNearestStation = {
  width: 180,
  title: i18n.t("global.nearestStation"),
  key: "nearestStation",
  dataIndex: "nearestStation",
};

Columns.companyListName = {
  width: 160,
  title: i18n.t("global.companyName"),
  key: "companyId",
  dataIndex: "name",
};

Columns.companyAddress = {
  width: 300,
  title: i18n.t("global.companyAddress"),
  key: "address",
  render: (record) => {
    const { city, buildingName, nearestStation, prefecture } = record;
    return (
      <Typography.Paragraph
        ellipsis={{
          rows: 3,
          expandable: true,
          onExpand: (e) => e.stopPropagation(),
        }}
      >
        {!_isEmpty(buildingName) ? `${buildingName},` : ""} {city},{" "}
        {nearestStation ? `${nearestStation},` : ""} {prefecture}
      </Typography.Paragraph>
    );
  },
};

Columns.customerName = {
  title: i18n.t("listUserScreen.customerName"),
  dataIndex: "displayName",
  key: "customerName",
  width: 180,
};

export default Columns;
