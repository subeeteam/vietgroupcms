import { object, string } from "yup";
import i18n from "i18n";

export const setTagValidate = object().shape({
  status: string().trim().required(i18n.t("errors.required")),
});
