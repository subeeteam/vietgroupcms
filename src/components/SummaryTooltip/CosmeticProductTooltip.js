import PropTypes from "prop-types";
import { Image, Row, Col } from "antd";
// import i18n from 'i18n';
import "./style.less";
import { PlaceHolderIMG } from "assets/images";

export const CosmeticProductTooltip = (props) => {
  const { cosmeticProductDetail = {} } = props;
  const {
    name,
    type = {},
    thumbnail,
    percentagePromotion,
    promotion,
    currency,
    element,
    amount,
  } = cosmeticProductDetail;
  return (
    <div className="cosmetic-product-summary-tooltip-wrapper">
      <div className="cosmetic-product-row">
        <div className="cosmetic-product-shop-thumbnail">
          <Image
            src={thumbnail}
            fallback={PlaceHolderIMG}
            alt=""
            width={110}
            height={90}
          />
          {(percentagePromotion > 0 || promotion > 0) && (
            <div className="promotion">
              -
              {percentagePromotion > 0
                ? `${percentagePromotion} %`
                : `${Intl.NumberFormat("de-DE").format(promotion)} ${currency}`}
            </div>
          )}
        </div>
        <Row
          className="cosmetic-product-shop-detail"
          justify="space-between"
          align="middle"
        >
          <Col>
            <div className="name-shop"> Tên sản phẩm</div>
            <div className="address-shop">{name}</div>
          </Col>
          <Col>
            <div className="name-shop">Loại</div>
            <div className="address-shop">{type.name}</div>
          </Col>
          <Col>
            <div className="name-shop">Số lượng</div>
            <div className="address-shop">{amount}</div>
          </Col>
          <Col>
            <div className="name-shop">Thành phần</div>
            <div className="address-shop">{element}</div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

CosmeticProductTooltip.propTypes = {
  cosmeticProductDetail: PropTypes.object,
};

export default CosmeticProductTooltip;
