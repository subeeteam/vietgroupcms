import PropTypes from "prop-types";
import { Image, Row, Col } from "antd";
// import i18n from 'i18n';
import "./style.less";

export const EntertainmentProductTooltip = (props) => {
  const { entertainmentProductDetail = {} } = props;
  const {
    name,
    productType = {},
    thumbnail,
    percentagePromotion,
    promotion,
    currency,
    element,
    amount,
  } = entertainmentProductDetail;
  return (
    <div className="entertainment-product-summary-tooltip-wrapper">
      <div className="entertainment-product-row">
        <div className="entertainment-product-shop-thumbnail">
          <Image src={thumbnail} alt="" width={110} height={90} />
          {(percentagePromotion > 0 || promotion > 0) && (
            <div className="promotion">
              -
              {percentagePromotion > 0
                ? `${percentagePromotion} %`
                : `${Intl.NumberFormat("de-DE").format(promotion)} ${currency}`}
            </div>
          )}
        </div>
        <Row
          className="entertainment-product-shop-detail"
          justify="space-between"
          align="middle"
        >
          <Col>
            <div className="name-shop"> Tên sản phẩm</div>
            <div className="address-shop">{name}</div>
          </Col>
          <Col>
            <div className="name-shop">Loại</div>
            <div className="address-shop">{productType.name}</div>
          </Col>
          <Col>
            <div className="name-shop">Số lượng</div>
            <div className="address-shop">{amount}</div>
          </Col>
          <Col>
            <div className="name-shop">Thành phần</div>
            <div className="address-shop">{element}</div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

EntertainmentProductTooltip.propTypes = {
  entertainmentProductDetail: PropTypes.object,
};

export default EntertainmentProductTooltip;
