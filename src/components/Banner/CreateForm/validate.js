import { object, string, array } from "yup";
import i18n from "i18n";

export const createBannerValidate = object().shape({
  url: string().trim().required(i18n.t("errors.required")),
  type: array().min(1, i18n.t("errors.required")),
  image: object().nullable().required(i18n.t("errors.required")),
});
