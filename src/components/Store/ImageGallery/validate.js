import { object, string } from "yup";
import i18n from "i18n";

export const createBannerValidate = object().shape({
  imageUrl: object().nullable().required(i18n.t("errors.required")),
  comment: string().trim().required(i18n.t("errors.required")),
});
