import { object, string } from "yup";
import i18n from "i18n";

export const simCustomField = object().shape({
  customOrderId: string().trim().required(i18n.t("errors.required")),
  phone: string().required(i18n.t("errors.required")),
});
