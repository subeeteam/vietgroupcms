/* eslint-disable react/display-name */

import { Row, Col, Tooltip } from "antd";
// import _get from 'lodash/get';
// import _isEmpty from 'lodash/isEmpty';
// import "./style.less";
import i18n from "i18n";
import moment from "moment";
import StatusBlock from "components/StatusBlock";
import ArticlePermissionTooltip from "components/ArticlePermissionTooltip";
import SummaryTooltip from "components/SummaryTooltip";
import VisaSummaryTooltip from "components/SummaryTooltip/VisaSummaryTooltip";

const Columns = {};

const renderDataValue = (value) => {
  const style = value ? {} : { color: "#c0c0c0" };
  return <span style={style}>{value || i18n.t("general.none")}</span>;
};

Columns.id = {
  dataIndex: "VisaId",
  key: "VisaId",
  title: "#",
  render: (VisaId, data) =>
    renderDataValue(
      <SummaryTooltip
        placement="bottomLeft"
        component={<VisaSummaryTooltip VisaDetail={data} />}
      >
        <div>#{VisaId}</div>
      </SummaryTooltip>
    ),
  width: 195,
};

Columns.createdAt = {
  dataIndex: "createdAt",
  key: "createdAt",
  title: i18n.t("visacreen.createAt"),
  width: 110,
  render: (createdAt, data) =>
    renderDataValue(
      <SummaryTooltip
        placement="bottomLeft"
        component={<VisaSummaryTooltip VisaDetail={data} />}
      >
        <div>{moment(createdAt).format("DD/MM/YYYY")}</div>
      </SummaryTooltip>
    ),
};

Columns.title = {
  title: i18n.t("visacreen.title"),
  dataIndex: "title",
  key: "title",
  sorter: true,
  render: (title, data) =>
    renderDataValue(
      <SummaryTooltip
        placement="bottomLeft"
        component={<VisaSummaryTooltip VisaDetail={data} />}
      >
        <div>{title}</div>
      </SummaryTooltip>
    ),
  width: 400,
};

Columns.username = {
  title: i18n.t("visacreen.account"),
  dataIndex: "idUser",
  key: "idUser",
  render: (idUser = {}, data) =>
    renderDataValue(
      <SummaryTooltip
        placement="bottomLeft"
        component={<VisaSummaryTooltip VisaDetail={data} />}
      >
        <div>{idUser.name}</div>
      </SummaryTooltip>
    ),
};
Columns.status = {
  title: i18n.t("visacreen.status"),
  dataIndex: "status",
  key: "status",
  width: 80,
  render: (status = []) =>
    renderDataValue(
      <Row>
        <Tooltip
          title={<ArticlePermissionTooltip active={status} />}
          color="white"
        >
          <Col style={{ marginRight: 4 }}>
            <StatusBlock status={status} />
          </Col>
        </Tooltip>
      </Row>
    ),
};

export default Columns;
