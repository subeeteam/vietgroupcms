/* eslint-disable react/display-name */

import i18n from "i18n";
import SummaryTooltip from "components/SummaryTooltip";
import CosmeticProductTooltip from "components/SummaryTooltip/CosmeticProductTooltip";
import ProductTag from "components/Product/Tag";

const Columns = {};
const renderDataValue = (value, renderStyle = {}) => {
  const style = value ? renderStyle : { ...renderStyle, color: "#c0c0c0" };
  return <span style={style}>{value || i18n.t("general.none")}</span>;
};

Columns.productId = {
  dataIndex: "cosmeticProductId",
  key: "cosmeticProductId",
  title: "#",
  render: (_id, data) =>
    renderDataValue(
      <SummaryTooltip
        placement="bottomLeft"
        component={<CosmeticProductTooltip cosmeticProductDetail={data} />}
        widthTooltipInner={680}
      >
        <div>{_id}</div>
      </SummaryTooltip>
    ),
  width: 100,
};

Columns.name = {
  title: i18n.t("cosmeticProduct.productName"),
  dataIndex: "name",
  key: "name",
  sorter: true,
  render: (state) => renderDataValue(state),
  width: 160,
};

Columns.type = {
  title: i18n.t("cosmeticProduct.type"),
  dataIndex: ["type", "name"],
  key: "type",
  sorter: true,
  render: (productType) => renderDataValue(productType),
  width: 110,
};

Columns.element = {
  title: i18n.t("cosmeticProduct.element"),
  dataIndex: "element",
  key: "element",
  sorter: true,
  render: (element) => renderDataValue(element),
  width: 140,
};

Columns.mass = {
  title: i18n.t("cosmeticProduct.mass"),
  dataIndex: "mass",
  key: "mass",
  sorter: true,
  render: (mass, record) =>
    renderDataValue(
      <div>
        {mass} {record.unit}
      </div>
    ),
  width: 120,
};

Columns.amount = {
  title: i18n.t("cosmeticProduct.amount"),
  dataIndex: "amount",
  key: "amount",
  sorter: true,
  width: 120,
  render: (amount) => renderDataValue(amount),
};

Columns.promotion = {
  title: i18n.t("cosmeticProduct.promotion"),
  dataIndex: "percentagePromotion",
  key: "percentagePromotion",
  width: 130,
  render: (percentagePromotion, record) =>
    renderDataValue(
      `${
        record.promotion !== 0
          ? `${record.promotion.toLocaleString()} ${record.currency}`
          : `${percentagePromotion}%`
      }`,
      { fontWeight: "bold" }
    ),
};

Columns.price = {
  title: i18n.t("cosmeticProduct.price"),
  dataIndex: "price",
  key: "price",
  width: 140,
  sorter: true,
  render: (price, data) =>
    renderDataValue(
      <div>
        {price?.toLocaleString()}
        {` ${data.currency}`}
      </div>
    ),
};

Columns.promotionPrice = {
  title: i18n.t("cosmeticProduct.promotionPrice"),
  dataIndex: "promotionPrice",
  key: "promotionPrice",
  width: 140,
  sorter: true,
  render: (promotionPrice, data) =>
    renderDataValue(
      <div>
        {promotionPrice?.toLocaleString()}
        {` ${data.currency}`}
      </div>
    ),
};

Columns.tag = {
  title: i18n.t("products.tag"),
  dataIndex: "tag",
  key: "tag",
  render: (tag) => renderDataValue(<ProductTag status={tag} />),
};

export default Columns;
