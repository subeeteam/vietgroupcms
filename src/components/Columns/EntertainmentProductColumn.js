/* eslint-disable react/display-name */

import i18n from "i18n";
import SummaryTooltip from "components/SummaryTooltip";
import EntertainmentProductTooltip from "components/SummaryTooltip/EntertainmentProductTooltip";
import ProductTag from "components/Product/Tag";

const Columns = {};
const renderDataValue = (value, renderStyle = {}) => {
  const style = value ? renderStyle : { ...renderStyle, color: "#c0c0c0" };
  return <span style={style}>{value || i18n.t("general.none")}</span>;
};

Columns.productId = {
  dataIndex: "productEntertainmentId",
  key: "productEntertainmentId",
  title: "#",
  render: (_id, data) =>
    renderDataValue(
      <SummaryTooltip
        placement="bottomLeft"
        component={
          <EntertainmentProductTooltip entertainmentProductDetail={data} />
        }
        widthTooltipInner={680}
      >
        <div>{_id}</div>
      </SummaryTooltip>
    ),
  width: 100,
};

Columns.name = {
  title: i18n.t("entertainmentProduct.productName"),
  dataIndex: "name",
  key: "name",
  sorter: true,
  render: (state) => renderDataValue(state),
  width: 200,
};

Columns.type = {
  title: i18n.t("entertainmentProduct.type"),
  dataIndex: ["productType", "name"],
  key: "productType",
  sorter: true,
  render: (productType) => renderDataValue(productType),
  width: 110,
};

Columns.element = {
  title: i18n.t("entertainmentProduct.element"),
  dataIndex: "element",
  key: "element",
  sorter: true,
  render: (element) => renderDataValue(element),
  width: 140,
};

Columns.mass = {
  title: i18n.t("entertainmentProduct.mass"),
  dataIndex: "mass",
  key: "mass",
  sorter: true,
  render: (mass, record) =>
    renderDataValue(
      <div>
        {mass} {record.unit}
      </div>
    ),
  width: 120,
};

Columns.amount = {
  title: i18n.t("entertainmentProduct.amount"),
  dataIndex: "amount",
  key: "amount",
  sorter: true,
  width: 120,
  render: (amount) => renderDataValue(amount),
};

Columns.promotion = {
  title: i18n.t("entertainmentProduct.promotion"),
  dataIndex: "percentagePromotion",
  key: "percentagePromotion",
  width: 130,
  render: (percentagePromotion, record) =>
    renderDataValue(
      `${
        record.promotion !== 0
          ? `${record.promotion.toLocaleString()} ${record.currency}`
          : `${percentagePromotion}%`
      }`,
      { fontWeight: "bold" }
    ),
};

Columns.price = {
  title: i18n.t("entertainmentProduct.price"),
  dataIndex: "price",
  key: "price",
  width: 140,
  sorter: true,
  render: (price, data) =>
    renderDataValue(
      <div>
        {price?.toLocaleString()}
        {` ${data.currency}`}
      </div>
    ),
};

Columns.promotionPrice = {
  title: i18n.t("entertainmentProduct.promotionPrice"),
  dataIndex: "promotionPrice",
  key: "promotionPrice",
  width: 140,
  sorter: true,
  render: (promotionPrice, data) =>
    renderDataValue(
      <div>
        {promotionPrice?.toLocaleString()}
        {` ${data.currency}`}
      </div>
    ),
};

Columns.tag = {
  title: i18n.t("products.tag"),
  dataIndex: "tag",
  key: "tag",
  render: (tag) => renderDataValue(<ProductTag status={tag} />),
};

export default Columns;
