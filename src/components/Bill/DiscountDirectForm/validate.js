import { object, string } from "yup";
import i18n from "i18n";

export const discountDirectValidate = object().shape({
  percentagePromotion: string().trim().required(i18n.t("errors.required")),
  promotion: string().trim().required(i18n.t("errors.required")),
});
