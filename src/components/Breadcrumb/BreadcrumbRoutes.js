import CompanynameBreadcrumb from "./CompanynameBreadcrumb";

const BreadcrumbRoutes = [
  { path: "/home", breadcrumb: null },
  { path: "/companies", breadcrumb: "Company" },
  { path: "/companies/:companyId", breadcrumb: CompanynameBreadcrumb },
];
export default BreadcrumbRoutes;
