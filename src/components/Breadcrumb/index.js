import { Link, useLocation } from "react-router-dom";
import { Breadcrumb } from "antd";
import useBreadcrumbs from "hooks/useBreadcrumb";
import BreadcrumbRoutes from "./BreadcrumbRoutes";

const Breadcrumbs = () => {
  const breadcrumbs = useBreadcrumbs(BreadcrumbRoutes);
  const location = useLocation();
  return (
    <Breadcrumb>
      {breadcrumbs.map(({ match, breadcrumb }) => (
        <Breadcrumb.Item key={match.pathname}>
          {location.pathname !== match.pathname ? (
            <Link to={match.pathname}>{breadcrumb}</Link>
          ) : (
            <>{breadcrumb}</>
          )}
        </Breadcrumb.Item>
      ))}
    </Breadcrumb>
  );
};

export default Breadcrumbs;
