import _ from "lodash";
import { useSelector } from "react-redux";
import {
  makeSelectCompanyDetail,
  makeSelectCompanyJobDetail,
} from "providers/CompanyProvider/selectors";

const CompanynameBreadcrumb = () => {
  const companyDetail = useSelector(makeSelectCompanyDetail()) || {};
  const jobDetail = useSelector(makeSelectCompanyJobDetail()) || {};
  const { company = {} } = jobDetail;
  return (
    <span>{_.get(companyDetail, "companyName", company.companyName)}</span>
  );
};
export default CompanynameBreadcrumb;
