import { useDispatch, useSelector } from "react-redux";
import _get from "lodash/get";
import { Modal, Row, Col, Image, Button } from "antd";
import { switchDetailModal } from "providers/RestaurantProductProvider/slice";
import moment from "moment";
import "./styles.less";
import { PlaceHolderIMG } from "assets/images";

const ProductDetailModal = () => {
  const dispatch = useDispatch();
  const modalData = useSelector((state) => state.marketProduct.detailModal);
  const isOpen = _get(modalData, "isOpen", false);
  const modalProps = _get(modalData, "data", {});
  const {
    thumbnail,
    name,
    type,
    productType,
    mass,
    unit,
    amount,
    inputPrice,
    currency,
    percentagePromotion,
    fromDatePromotion,
    promotion = 0,
    toDatePromotion,
    okText,
    cancelText,
    okType,
    okButtonProps,
    onOk,
    onCancel,
    closeAfterConfirm = true,
    content,
    ...props
  } = modalProps;
  const handleCancel = () => {
    if (typeof onCancel === "function") {
      onCancel();
    }
    dispatch(switchDetailModal({ isOpen: false }));
  };

  const handleConfirm = () => {
    if (typeof onOk === "function") {
      onOk();
    }
    if (closeAfterConfirm) {
      // Close popup first
      dispatch(switchDetailModal({ isOpen: false }));
    }
  };

  return (
    <Modal
      title="Sản phẩm chi tiết"
      visible={isOpen}
      width={945}
      afterClose={() => dispatch(switchDetailModal({ data: {} }))} // Remove popup data when popup is close
      footer={null}
      onCancel={handleCancel}
      onOk={handleConfirm}
      cancelText={cancelText}
      okText={okText}
      okType={okType}
      okButtonProps={{ size: "large", ...okButtonProps }}
      cancelButtonProps={{ size: "large", className: "cancel-button" }}
      centered
      className="product-detail-modal"
      {...props}
    >
      <Row>
        <Col className="image-container" style={{ maxWidth: 210 }}>
          <Image
            src={thumbnail}
            fallback={PlaceHolderIMG}
            alt=""
            width={210}
            height={165}
          />
        </Col>
        <Col flex="auto">
          <div className="m-title-text">Thông tin cơ bản</div>
          <Row className="detail-row">
            <Col span={6}>
              <div className="s-text bold">Tên sản phẩm:</div>
              <div className="detail-text">{name}</div>
            </Col>
            <Col span={6}>
              <div className="s-text bold">Loại:</div>
              <div className="detail-text">{_get(productType, "name")}</div>
            </Col>
            <Col span={6}>
              <div className="s-text bold">Khối lượng:</div>
              <div className="detail-text">
                {mass} {unit}
              </div>
            </Col>
            <Col span={6}>
              <div className="s-text bold">Số lượng:</div>
              <div className="detail-text">{amount}</div>
            </Col>
          </Row>
          <Row className="detail-row">
            <Col span={6}>
              <div className="s-text bold">Giá bán:</div>
              <div className="detail-text">
                {inputPrice && inputPrice.toLocaleString()} {currency}
              </div>
            </Col>
            <Col span={6}>
              <div className="s-text bold">Khuyến mãi:</div>
              <div className="detail-text">
                {percentagePromotion > 0
                  ? `${percentagePromotion}%`
                  : `${promotion} ${currency}`}
              </div>
            </Col>
            <Col span={6}>
              <div className="s-text bold">Từ ngày:</div>
              <div className="detail-text">
                {fromDatePromotion &&
                  moment(fromDatePromotion).format("DD/MM/YYYY")}
              </div>
            </Col>
            <Col span={6}>
              <div className="s-text bold">Đến ngày:</div>
              <div className="detail-text">
                {toDatePromotion &&
                  moment(toDatePromotion).format("DD/MM/YYYY")}
              </div>
            </Col>
          </Row>
          <Row className="button-wrapper" justify="space-between">
            <Button type="primary">CHỈNH SỬA</Button>
            <Button className="cancel-button">XÓA SẢN PHẨM</Button>
          </Row>
        </Col>
      </Row>
    </Modal>
  );
};

export default ProductDetailModal;
