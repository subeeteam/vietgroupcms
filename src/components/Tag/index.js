import PropTypes from "prop-types";
import { USER_STATUS } from "utils/constants";
import "./style.less";
import i18n from "i18n";

const statusClassName = {
  [USER_STATUS.ACTIVE]: "active-tag",
};

const statusText = {
  [USER_STATUS.ACTIVE]: i18n.t("listUserScreen.active"),
};

const Tag = ({ status }) => (
  <div className={`tag-container ${statusClassName[status]}`}>
    <div className="status-text">{statusText[status]}</div>
  </div>
);

Tag.propTypes = {
  status: PropTypes.string,
};
export default Tag;
