import { object, string } from "yup";
import i18n from "i18n";

export const createBannerValidate = object().shape({
  link: string().trim().required(i18n.t("errors.required")),
  imageUrl: object().nullable().required(i18n.t("errors.required")),
});
