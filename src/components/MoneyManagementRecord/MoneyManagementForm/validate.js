import { object, string } from "yup";
import i18n from "i18n";

export const moneyManagementValidate = object().shape({
  type: string().trim().required(i18n.t("errors.required")),
  payment: string().trim().required(i18n.t("errors.required")),
  value: string().trim().required(i18n.t("errors.required")),
  description: string().trim().required(i18n.t("errors.required")),
});
