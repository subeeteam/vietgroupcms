import { useRef, useEffect, useCallback } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import { useFormikContext } from "formik";
import usePrevious from "./usePrevious";

const FormikOnChange = ({ delay, onChange }) => {
  const { values } = useFormikContext();
  const prevValues = usePrevious(values);
  const isFirstRun = useRef(true);
  const debouncedOnChange = useCallback(_.debounce(onChange, delay), []);

  useEffect(() => {
    if (!_.isEqual(values, prevValues)) {
      if (isFirstRun.current) {
        isFirstRun.current = false;

        return;
      }

      if (delay > 0) {
        debouncedOnChange(values);
      } else {
        onChange(values);
      }
    }
  }, [values]);
  return null;
};

FormikOnChange.propTypes = {
  delay: PropTypes.number, // ms
  onChange: PropTypes.func.isRequired,
};

FormikOnChange.defaultProps = {
  delay: 0,
};

export default FormikOnChange;
